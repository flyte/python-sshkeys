#!/usr/bin/python

from restkit import request
import simplejson as json
import argparse, sys

parser = argparse.ArgumentParser(description="Get the SSH keys for the users in a certain group.")
parser.add_argument("group", type=int, metavar='group', help="Numerical ID of the group in the database")

args = parser.parse_args()
url = "http://192.168.1.1:8000"

try:
	req = request("%s/api/v1/membership/?format=json&group=%s" % (url, args.group))
except Exception, e:
	print "Error: %s" %str(e)
	sys.exit(1)

obj = json.loads(req.body_string())

for o in obj["objects"]:
	req = request("%s%s?format=json" %(url, o["user"]))
	user = json.loads(req.body_string())
	print user["public_key"]
