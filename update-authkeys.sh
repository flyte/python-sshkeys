#!/bin/sh

echo "Attempting to connect to database..."
keys=`/home/user/python-sshkeys/getkeys.py $1`
if [ $? -eq 0 ]; then
	echo "Got list of keys from database."
else
	echo "Connection failed. Exiting."
	exit 2
fi

echo -n "Updating authorized_keys file..."
echo $keys > /home/user/.ssh/authorized_keys
echo "DONE"
