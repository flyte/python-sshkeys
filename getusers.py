#!/usr/bin/python

from restkit import request
import simplejson as json

resp = request("http://192.168.1.1:8000/api/v1/user/?format=json")
obj = json.loads(resp.body_string())

for o in obj["objects"]:
	print "############################################"
	print "Name: %s %s" %(o["firstname"], o["surname"])
	print "Device: %s" %o["device"]
	print "Phone: %s" %o["phone"]
	print "Public key: %s" %o["public_key"]
	print "############################################"
